<?php
    require_once __DIR__."/../inc/conf.inc.php";

    class DBSingleton {

        private $con;
        static $_instance;

        private function __construct() {}

        private function __clone(){}

        public static function getInstance() {
            if (!(self::$_instance instanceof self)) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }

        private static function initConnection(){
            $db = self::getInstance();
           
            $db->dbConn = new PDO("mysql:host=".HOST.";dbname=".DBNAME.";charset=utf8",USER,PASS);
            $db->dbConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          
            return $db;
        }

        public static function getDbConn() {
            try {
                $db = self::initConnection();

                return $db->dbConn;
            } catch (Exception $ex) {
                echo "No se ha podido conectar a la base de datos. " . $ex->getMessage();
                return null;
            }
        }
    }
?>
