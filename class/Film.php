<?php
	require_once 'DBSingleton.php';

	class Film {
		private $film_id;
		private $title;
		private $description;
		private $release_year;
		private $language_id;
		private $original_language_id;
		private $rental_duration;
		private $rental_rate;
		private $length;
		private $replacement_cost;
		private $rating;
		private $special_features;
		private $image;
		private $last_update;
		
		public function __construct($film_id=null,$title=null,$description=null,$release_year=null,$language_id=null,$original_language_id=null,$rental_duration=null,$rental_rate=null,$length=null,$replacement_cost=null,$rating=null,$special_features=null,$image=null)
		{
			$this->setFilmId($film_id);
			$this->setTitle($title);
			$this->setDescription($description);
			$this->setReleaseYear($release_year);
			$this->setLanguageId($language_id);
			$this->setOriginalLanguageId($original_language_id);
			$this->setRentalDuration($rental_duration);
			$this->setRentalRate($rental_rate);
			$this->setLength($length);
			$this->setReplacementCost($replacement_cost);
			$this->setRating($rating);
			$this->setSpecialFeatures($special_features);
			$this->setImage($image);
		}

		public function __toString(){
        	return $this->getVariablesFromFilmIntoArray();
   		}

		 //GETTER Y SETTER
		public function setFilmId($fimlId){
			$this->film_id=$fimlId;
		}

		public function getFilmId(){
			return $this->film_id;
		}

		public function setTitle($title){
			if(strlen($title)>255){
				$this->title=substr($title,0,255);
			}else{
				$this->title=$title;
			}
		}

		public function getTitle(){
			return $this->title;
		}

		public function setDescription($description){
			$this->description=$description;
		}

		public function getDescription(){
			return $this->description;
		}

		public function setReleaseYear($release_year){
			if(strlen($release_year)>4){
				$this->release_year=0000;
			}else{
				$this->release_year=$release_year;
			}
			
		}

		public function getReleaseYear(){
			return $this->release_year;
		}

		public function setLanguageId($language_id){
			if($language_id>999){
				$this->language_id=999;
			}else{
				$this->language_id=$language_id;
			}
		}

		public function getLanguageId(){
			return $this->language_id;
		}

		public function setOriginalLanguageId($original_language_id){
			if($original_language_id>999){
				$this->original_language=999;
			}else{
				$this->original_language_id=$original_language_id;
			}	
		}

		public function getOriginalLanguageId(){
			return $this->original_language_id;
		}

		public function setRentalDuration($rental_duration){
			if($rental_duration>999){
				$this->rental_duration=999;
			}else{
				$this->rental_duration=$rental_duration;
			}	
		}

		public function getRentalDuration(){
			return $this->rental_duration;
		}

		public function setRentalRate($rental_rate){
			if($rental_rate>9999.99){
				$this->rental_rate=9999.99;
			}else{
				$this->rental_rate=$rental_rate;	
			}	
		}

		public function getRentalRate(){
			return $this->rental_rate;
		}

		public function setLength($length){
			if($length>99999){
				$this->length=99999;
			}else{
				$this->length=$length;
			}	
		}

		public function getLength(){
			return $this->length;
		}

		public function setReplacementCost($replacement_cost){
			if($replacement_cost>99999.99){
				$this->replacement_cost=99999.99;
			}else{
				$this->replacement_cost=$replacement_cost;
			}
		}

		public function getReplacementCost(){
			return $this->replacement_cost;
		}

		public function setRating($rating){
			if(is_nan($rating)){
				$this->rating=$rating;
			}else{
				$this->rating="";
			}
		}

		public function getRating(){
			return $this->rating;
		}

		public function setSpecialFeatures($special_features){
			$this->special_features=$special_features;
		}

		public function getSpecialFeatures(){
			return $this->special_features;
		}

		public function setImage($image){
			if(strlen($image)>255){
				$this->image=substr($iamge,0,255);
			}else{
				$this->image=$image;
			}
		}

		public function getImage(){
			return $this->image;
		}

		public function getLastUpdate(){
			return $this->last_update;
		}

		//FIN GETTER Y SETTER

		//INICIO DE MÉTODOS DE LA CLASE
		//Este método devuelve un array que contiene todas las variables de una película que se le pase como parámetro.
		public function getVariablesFromFilmIntoArray($boolean){
			$film_variables_array = array();

			if($boolean === false){
				$film_variables_array[":film_id"] = $this->getFilmId();
			}
			$film_variables_array[":title"] = $this->getTitle();
			$film_variables_array[":description"] = $this->getDescription();
			$film_variables_array[":release_year"] = $this->getReleaseYear();
			$film_variables_array[":language_id"] = $this->getLanguageId();
			$film_variables_array[":original_language_id"] = $this->getOriginalLanguageId();
			$film_variables_array[":rental_duration"] = $this->getRentalDuration();
			$film_variables_array[":rental_rate"] = $this->getRentalRate();
			$film_variables_array[":length"] = $this->getLength();
			$film_variables_array[":replacement_cost"] = $this->getReplacementCost();
			$film_variables_array[":rating"] = $this->getRating();
			$film_variables_array[":special_features"] = $this->getSpecialFeatures();
			$film_variables_array[":image"] = $this->getImage();

			return $film_variables_array;
		}

		//Este método devuelve un array con todas las películas que cumplen los filtros que se le pasan por parámetro.
		public static function getArrayFilms ($arrFilters, $paginated, $start,$numFields) {
			$con = DBSingleton::getDbConn();
			$sql = "SELECT * FROM film WHERE TRUE ";
	
			if(!empty($arrFilters)){	

				if(isset($arrFilters[":film_id"]) && !empty($arrFilters[":film_id"])){
					$sql .= " and film_id = :film_id ";
				}else{
					unset($arrFilters[":film_id"]);
				}
				if(isset($arrFilters[":title"]) && !empty($arrFilters[":title"])){
					$arrFilters[":title"] = $arrFilters[":title"]."%";
					$sql .= " and title LIKE :title";
				}else{
					unset($arrFilters[":title"]);
				}
				
				if(isset($arrFilters[":description"]) && !empty($arrFilters[":description"])){
				 $sql .= " and description like :description ";
				}else{
					unset($arrFilters[":description"]);
				}

				if(isset($arrFilters[":release_year"]) && !empty($arrFilters[":release_year"])){
				 $sql .= " and release_year = :release_year ";
				}else{
				 	unset($arrFilters[":release_year"]);
				 }

				if(isset($arrFilters[":language_id"]) && !empty($arrFilters[":language_id"])){
				 $sql .= " and language_id = :language_id ";
				}else{
				 	unset($arrFilters[":language_id"]);
				 }

				if(isset($arrFilters[":original_language_id"]) && !empty($arrFilters[":original_language_id"])){
				$sql .= " and original_language_id = :original_language_id";
				}else{
					unset($arrFilters[":original_language_id"]);
				}
				if(isset($arrFilters[":rental_duration"]) && !empty($arrFilters[":rental_duration"])){
				$sql .= " and rental_duration = :rental_duration ";
				}else{
					unset($arrFilters[":rental_duration"]);
				}
				if(isset($arrFilters[":rental_rate"]) && !empty($arrFilters[":rental_rate"])){
				$sql .= " and rental_rate = :rental_rate ";
				}else{
					unset($arrFilters[":rental_rate"]);
				}
				if(isset($arrFilters[":length"]) && !empty($arrFilters[":length"])){
				$sql .= " and length = :length ";
				}else{
					unset($arrFilters[":length"]);
				}
				if(isset($arrFilters[":replacement_cost"]) && !empty($arrFilters[":replacement_cost"])){
				$sql .= " and replacement_cost = :replacement_cost ";
				}else{
					unset($arrFilters[":replacement_cost"]);
				}
				if(isset($arrFilters[":rating"]) && !empty($arrFilters[":rating"])){
				$sql .= " and rating = :rating ";
				}else{
					unset($arrFilters[":rating"]);
				}
				if(isset($arrFilters[":special_features"]) && !empty($arrFilters[":special_features"])){
				$sql .= " and special_features like :special_features ";
				}else{
					unset($arrFilters[":special_features"]);
				}
				if(isset($arrFilters[":image"]) && !empty($arrFilters[":image"])){
				$sql .= " and image like :image ";
				}else{
					unset($arrFilters[":image"]);
				}
				if(isset($arrFilters[":last_update"]) && !empty($arrFilters[":last_update"])){
				$sql .= " and last_update = :last_update ";
				}else{
					unset($arrFilters[":last_update"]);	
				}

				if($paginated===true){
					$calculo = $start * $numFields;
					$sql .= " limit $calculo,$numFields";
				}

				$stmt = $con->prepare($sql);
				$stmt->execute($arrFilters);
			}else{
				if($paginated===true){
					$calculo = $start * $numFields;
					$sql .= " limit $calculo,$numFields";
				}
				$stmt = $con->prepare($sql);
				$stmt->execute();
			}

		   	$peliculasBD = $stmt->fetchAll(PDO::FETCH_OBJ);
		   	$peliculasArray = array();

			foreach ($peliculasBD as $pelicula){
				array_push($peliculasArray,$pelicula);				
			}
			return $peliculasArray;
		}

		//Este método borra una película en función de su id.
		public function borrarPelicula() {
			$con = DBSingleton::getDbConn();
			$stmt=$con->prepare("DELETE FROM film where film_id=?;");
			$stmt->execute([$this->getFilmId()]);
		}

		//Este método inserta la película que se le pasa como parámetro,el film_id no se le asigna,debido a que en la base de datos es autoincremental.
		public function insertarPelicula(){
			$con = DBSingleton::getDbConn();
			$data = $this->getVariablesFromFilmIntoArray(true);

			$stmt=$con->prepare("INSERT INTO film (title,description,release_year,language_id,original_language_id,rental_duration,rental_rate,length,replacement_cost,rating,special_features,image,last_update) 
				 VALUES (:title,:description,:release_year,:language_id,:original_language_id,:rental_duration,:rental_rate,:length,:replacement_cost,:rating, :special_features,:image,CURRENT_TIMESTAMP);");

			$stmt->execute($data);

		}

		//Este método modifica una película reemplazando todos sus campos por las propiedades de la película que se le pasa por parámetros a excepcion del film_id, que no se modifica.
		public function modificarPelicula(){
			$con = DBSingleton::getDbConn();
			$data = $this->getVariablesFromFilmIntoArray(false);
			$stmt=$con->prepare("UPDATE film SET title = :title,description = :description,release_year = :release_year,language_id = :language_id,original_language_id = :original_language_id,rental_duration = :rental_duration,rental_rate = :rental_rate,length = :length,replacement_cost = :replacement_cost,rating = :rating,special_features = :special_features,image = :image,last_update = CURRENT_TIMESTAMP WHERE film_id = :film_id;");

			$stmt->execute($data);
		}

		public static function getFilmCount(){
			$con = DBSingleton::getDbConn();
			$stmt=$con->prepare("SELECT COUNT(*) FROM film");
			$stmt->execute();
			return $stmt->fetch();
		}
	}
?>