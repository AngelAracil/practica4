<?php
	require_once "class/Film.php";
	
	//Recolección de variables por el método POST
	$action = isset($_POST['action'])? $_POST['action']:'json';
	$paginated = isset($_POST['paginated'])? $_POST['paginated']:true;
	if(isset($_POST['start']) && $_POST["start"]>=0){
		$start=$_POST['start'];
	}else{
		$start=0;
	}
	if(isset($_POST['numFields'])){
		$numFields=$_POST['numFields'];
	}else{
		$numFields=10;
	}
	$arrFilters = isset($_POST['arrFilters'])? $_POST['arrFilters']:array();
	$arrData = isset($_POST['arrData'])? $_POST['arrData']:array();
	
	//Convierto a booleano paginated si me lo envian como string.
	if ($paginated == 'true' || $paginated === true){
		$paginated = true;
	}else{
		$paginated = false;
	}

	try {
		switch ($action) {
			case 'get':
				$json = json_encode(array(
					"success"=>true,
					"msg"=>"Peliculas recogidas.",
					"data"=>Film::getArrayFilms($arrFilters, $paginated, $start,$numFields)));
			break;

			case 'delete':
				
				$film = new Film($arrData["film_id"]);
				$film->borrarPelicula();
				$json = json_encode(array(
					"success"=>true,
					"msg"=>"Pelicula borrada."));
			break;

			case 'insert':
			
				if(empty($arrData["title"])) $arrData["title"]=null;
				if(empty($arrData["description"])) $arrData["description"]=null;
				if(empty($arrData["release_year"])) $arrData["release_year"]=null;
				if(empty($arrData["language_id"])) $arrData["language_id"]=null;
				if(empty($arrData["original_language_id"])) $arrData["original_language_id"]=null;
				if(empty($arrData["rental_duration"])) $arrData["rental_duration"]=null;
				if(empty($arrData["rental_rate"])) $arrData["rental_rate"]=null;
				if(empty($arrData["length"])) $arrData["length"]=null;
				if(empty($arrData["replacement_cost"])) $arrData["replacement_cost"]=null;
				if(empty($arrData["rating"])) $arrData["rating"]=null;
				if(empty($arrData["special_features"])) $arrData["special_features"]=null;
				if(empty($arrData["image"])) $arrData["image"]=null;

			 	$film = new Film(null,$arrData["title"],$arrData["description"],$arrData["release_year"],$arrData["language_id"],$arrData["original_language_id"],$arrData["rental_duration"],$arrData["rental_rate"],$arrData["length"],$arrData["replacement_cost"],$arrData["rating"],$arrData["special_features"],$arrData["image"],null);
			 	$film->insertarPelicula();
				$json = json_encode(array(
					"success"=>true,
					"msg"=>"Pelicula insertada."));
			break;

			case 'update':

				if(empty($arrData["title"])) $arrData["title"]=null;
				if(empty($arrData["description"])) $arrData["description"]=null;
				if(empty($arrData["release_year"])) $arrData["release_year"]=null;
				if(empty($arrData["language_id"])) $arrData["language_id"]=null;
				if(empty($arrData["original_language_id"])) $arrData["original_language_id"]=null;
				if(empty($arrData["rental_duration"])) $arrData["rental_duration"]=null;
				if(empty($arrData["rental_rate"])) $arrData["rental_rate"]=null;
				if(empty($arrData["length"])) $arrData["length"]=null;
				if(empty($arrData["replacement_cost"])) $arrData["replacement_cost"]=null;
				if(empty($arrData["rating"])) $arrData["rating"]=null;
				if(empty($arrData["special_features"])) $arrData["special_features"]=null;
				if(empty($arrData["image"])) $arrData["image"]=null;

				$film = new Film($arrData["film_id"],$arrData["title"],$arrData["description"],$arrData["release_year"],$arrData["language_id"],$arrData["original_language_id"],$arrData["rental_duration"],$arrData["rental_rate"],$arrData["length"],$arrData["replacement_cost"],$arrData["rating"],$arrData["special_features"],$arrData["image"]);
			 	$film->modificarPelicula();
				$json = json_encode(array(
					"success"=>true,
					"msg"=>"Pelicula modificada."));
			break;

			case 'getFilmCount':
				$json = json_encode(array(
					"success"=>true,
					"msg"=>"Número de películas recogido.",
					"data"=>Film::getFilmCount()));
			break;

			default:
				$json = json_encode(array(
					"success"=>false,
					"msg"=>"Formato no soportado."));
			break;
		}	
	}catch(Exception $e){
		$json = json_encode(array(
			"success"=>false,
			"msg"=>$e->getMessage()
		));
	}

	echo $json;
	exit();
?>