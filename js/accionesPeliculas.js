
$(document).ready(getFilmCount());
$(document).ready(getFilms());

function deleteFilms(film_id){
	if (confirm("ALERTA!! va a proceder a eliminar una película, si desea eliminarla de click en ACEPTAR\n de lo contrario de click en CANCELAR.")) {

		$.ajax({
			method: "POST",
			url: "film_sw.php",
			data: {
			action: "delete",
			arrData:{"film_id":film_id}
			},
			dataType: "json"
			})

		.done(function( response ) {
			alert(response.msg);
			getFilms();
		})

		.fail(function( jqXHR, textStatus, errorThrown ) {
			alert( "Error: " + textStatus );
		});
	}else {
		return true;
	}
}

function sendValueToModal(film_id,title,description,release_year,language_id,original_language_id,rental_duration,rental_rate,length,replacement_cost,rating,special_features,image){
	$("#hiddenInput").val(film_id);
	$("#form_title").val(title);
	$("#form_description").val(description);
	$("#form_release_year").val(release_year);
	$("#form_language_id").val(language_id);
	$("#form_original_language_id").val(original_language_id);
	$("#form_rental_duration").val(rental_duration);
	$("#form_rental_rate").val(rental_rate);
	$("#form_length").val(length);
	$("#form_replacement_cost").val(replacement_cost);
	$("#form_rating").val(rating);
	$("#form_special_features").val(special_features);
	$("#form_image").val(image);
}

function updateFilms(){
	
	$.ajax({
		method: "POST",
		url: "film_sw.php",
		data: {
		action: "update",
		arrData:{
			"film_id":$("#hiddenInput").val(),
			"title":$("#form_title").val(),
			"description":$("#form_description").val(),
			"release_year":$("#form_release_year").val(),
			"language_id":$("#form_language_id").val(),
			"original_language_id":$("#form_original_language_id").val(),
			"rental_duration":$("#form_rental_duration").val(),
			"rental_rate":$("#form_rental_rate").val(),
			"length":$("#form_length").val(),
			"replacement_cost":$("#form_replacement_cost").val(),
			"rating":$("#form_rating").val(),
			"special_features": $("#form_special_features").val(),
			"image":$("#form_image").val()}
		},
		dataType: "json"
		})

	.done(function( response ) {
		alert(response.msg);
		getFilms();
	})

	.fail(function( jqXHR, textStatus, errorThrown ) {
		alert( "Error: " + textStatus );
	});
}

function insertFilm(){
	if($("#hiddenInput").val() == "Insertar Pelicula"){

		$.ajax({
			method: "POST",
			url: "film_sw.php",
			data: {
			action: "insert",
			arrData:{
				"title":$("#form_title").val(),
				"description":$("#form_description").val(),
				"release_year":$("#form_release_year").val(),
				"language_id":$("#form_language_id").val(),
				"original_language_id":$("#form_original_language_id").val(),
				"rental_duration":$("#form_rental_duration").val(),
				"rental_rate":$("#form_rental_rate").val(),
				"length":$("#form_length").val(),
				"replacement_cost":$("#form_replacement_cost").val(),
				"rating":$("#form_rating").val(),
				"special_features": $("#form_special_features").val(),
				"image":$("#form_image").val()}
			},
			dataType: "json"
			})

		.done(function( response ) {
			alert(response.msg);
		})

		.fail(function( jqXHR, textStatus, errorThrown ) {
			alert( "Error: " + textStatus );
		});
	}else{
		updateFilms();
	}	
}

function getFilmCount (){

	$.ajax({
		method: "POST",
		url: "film_sw.php",
		data: {action: "getFilmCount"},
		dataType: "json"
		})

	.done(function( response ) {
		$("#paginador_ultimo").val(response.data["COUNT(*)"]);
	})

	.fail(function( jqXHR, textStatus, errorThrown ) {
		alert( "Error: " + textStatus );
	});
}

function getFilms(paginator_buttons_action){	

	var paginator_start = parseInt($("#paginador_start").val());
	var num_fields = $("#paginador_numFields").val();
	var paginated = true;

	if((parseInt(num_fields)<0 || isNaN(num_fields) ) && (num_fields!="todos") || paginator_buttons_action!=null){
		num_fields=10;
		$("#paginador_numFields").val(10);
	}else if(num_fields=="todos"){
		paginated = false;
		$("#paginador_numFields").val("todos");
	}else{
		$("#paginador_numFields").val(num_fields);
	}

	if(paginator_buttons_action !=null){
		if (paginator_buttons_action== "primero"){
			paginator_start = 0;
		}else if (paginator_buttons_action == "ultimo"){
			paginator_start = Math.floor(parseInt($("#paginador_ultimo").val())/num_fields);
		}else{
			paginator_start = parseInt(paginator_start) + parseInt(paginator_buttons_action);
		}
		if(paginator_start>=Math.floor(parseInt($("#paginador_ultimo").val())/num_fields)){
				paginator_start=Math.floor(parseInt($("#paginador_ultimo").val())/num_fields);
		}
	}
	if(parseInt(paginator_start)<0 || isNaN(paginator_start)){
		paginator_start=0;
		$("#paginador_start").val(0);
	}else{
		$("#paginador_start").val(paginator_start);
	}
	

	$("#tablaPeliculas tbody").empty();
	$.ajax({
		method: "POST",
		url: "film_sw.php",
		data: {
			action: "get",
			start: paginator_start,
			numFields: num_fields,
			paginated: paginated,
			arrFilters:{
			":film_id":$("#filter_film_id").val(),
			":title":$("#filter_title").val(),
			":description":$("#filter_description").val(),
			":release_year":$("#filter_release_year").val(),
			":language_id":$("#filter_language_id").val(),
			":original_language_id":$("#filter_original_language_id").val(),
			":rental_duration":$("#filter_rental_duration").val(),
			":rental_rate":$("#filter_rental_rate").val(),
			":length":$("#filter_length").val(),
			":replacement_cost":$("#filter_replacement_cost").val(),
			":rating":$("#filter_rating").val(),
			":special_features": $("#filter_special_features").val(),
			":image":$("#filter_image").val(),
			":last_update":$("#filter_last_update").val()}
			},
		dataType: "json"
		})

	.done(function( response ) {
		$("#msg").html(response.msg);
		
		var miArray = response.data;
		miArray.forEach( function(valor, indice, array) {
		$("#tablaPeliculas").append("<tr class='columnaDeDatos'>"+
			"<td><button data-toggle='modal' data-target='#myModal' onclick='sendValueToModal("+valor.film_id+",\""+valor.title+"\","+"\""+valor.description+"\","+valor.release_year+","+valor.language_id
			+","+valor.original_language_id+","+valor.rental_duration+","+valor.rental_rate+","+valor.length+","+valor.replacement_cost+","+"\""+valor.rating+"\""+","+"\""+valor.special_features+"\""+","+"\""+valor.image+"\""+","+"\""+valor.last_update+"\")"+"'>Modificar</button></td>"+
			"<td>"+valor.film_id+"</td>"+
			"<td>"+valor.title+"</td>"+
			"<td>"+valor.description+"</td>"+
			"<td>"+valor.release_year+"</td>"+
			"<td>"+valor.language_id+"</td>"+
			"<td>"+valor.original_language_id+"</td>"+
			"<td>"+valor.rental_duration+"</td>"+
			"<td>"+valor.rental_rate+"</td>"+
			"<td>"+valor.length+"</td>"+
			"<td>"+valor.replacement_cost+"</td>"+
			"<td>"+valor.rating+"</td>"+
			"<td>"+valor.special_features+"</td>"+
			"<td>"+valor.image+"</td>"+
			"<td>"+valor.last_update+"</td>"+
			"<td><button onclick='deleteFilms("+valor.film_id+")'>Borrar</button></td>"+
			"</tr>");
		});
	})

	.fail(function( jqXHR, textStatus, errorThrown ) {
		alert( "Error: " + textStatus );
	});		
}